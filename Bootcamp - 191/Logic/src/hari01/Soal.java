package hari01;


public class Soal {

	public static void main(String[] args) {
		System.out.print("Soal 1 : "); soal01();
		System.out.println("");
		System.out.print("Soal 2 : "); soal02();
		System.out.println("");
		System.out.print("Soal 3 : "); soal03();
		System.out.println("");
		System.out.print("Soal 4 : "); soal04();
		System.out.println("");
		System.out.print("Soal 5 : "); soal05();
		System.out.println("");
		System.out.print("Soal 6 : "); soal06();
		System.out.println("");
		System.out.print("Soal 7 : "); soal07();
		System.out.println("");
		System.out.print("Soal 8 : "); soal08();
		System.out.println("");
		System.out.print("Soal 9 : "); soal09();
		System.out.println("");
		System.out.print("Soal 10 : "); soal10();
		}
		
	public static void soal01() {
		int c = 1;
		for (int i=0; i<7; i++) {
			System.out.print(c+" ");
			c+=2;
		}
	}
	
	public static void soal02() {
		int c = 2;
		for (int i=0; i<7; i++) {
			System.out.print(c+" ");
			c+=2;
		}
	}
	
	public static void soal03() {
		int c = 1;
		for (int i=0; i<7; i++) {
			System.out.print(c+" ");
			c+=3;
		}
	}
	
	public static void soal04() {
		int c = 1;
		for (int i=0; i<7; i++) {
			System.out.print(c+" ");
			c+=3;
		}
	}
	
	public static void soal05() {
		int c = 1;
		int d = 7;
		for (int i=0; i<d; i++) {
			if(i == 2) {
				System.out.print("*"+" ");
			}else if (i == 5) {
				System.out.print("*"+" ");
			}else {
				System.out.print(c+" ");
				c+=4;
			}	
		}
	}
	
	public static void soal06() {
		int c = 1;
		int d = 7;
		for (int i=0; i<d; i++) {
			if(i == 2) {
				System.out.print("*"+" ");
			}else if (i == 5) {
				System.out.print("*"+" ");
			}else {
				System.out.print(c+" ");
			}
			c+=4;	
		}
	}
	
	public static void soal07() {
		int c = 2;
		for (int i=0; i<7; i++) {
				System.out.print(c+" ");
				c *= 2; 
			}	
		}
	
	public static void soal08() {
		int c = 2;
		for (int i=0; i<7; i++) {
				System.out.print(c+" ");
				c *= 3; 
			}	
		}
	public static void soal09() {
		int c = 4;
		for (int i=0; i<7; i++) {
			if(i==2){
				System.out.print("*"+" ");
			} else if (i==5) {
				System.out.print("*"+" ");
			} else {
				System.out.print(c+" ");
				c *= 4; 
			}
		}
		}
	
	public static void soal10() {
		int c = 3;
		for (int i=0; i<7; i++) {
			if (i==3) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(c+" ");
			}
			c *= 3;
		}
		}

}

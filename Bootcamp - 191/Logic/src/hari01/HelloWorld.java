package hari01;

public class HelloWorld {
	
	private static int batch;
	
	public static void main(String[] args) {
		System.out.println("Welcome To Java");
		makananFavorit();
		sampleObject();
	}
	
	public static void sampleObject() {
		int anu = 10;
		Orang or01 = new Orang();
		or01.nama="Via";
		or01.alamat="Magelang";
		or01.jk="wanita";
		or01.tptlahir="Lampung";
		or01.umur=22;
		or01.cetak();
		
		Orang or02 = new Orang();
		or02.nama="Badung";
		or02.alamat="Jakarta";
		or02.jk="Laki-laki";
		or02.tptlahir="Jakarta";
		or02.umur=24;
		or02.cetak();
		
		Orang or03 = new Orang();
		or03.nama="Bahar";
		or03.alamat="Depok";
		or03.jk="Laki-laki";
		or03.tptlahir="Depok";
		or03.umur=30;
		or03.cetak();
		
		Orang or04 = new Orang();
		or04.nama="Munik";
		or04.alamat="Bogor";
		or04.jk="Laki-laki";
		or04.tptlahir="Bogor";
		or04.umur=25;
		or04.cetak();

		Orang or05 = new Orang();
		or05.nama="Keleo";
		or05.alamat="Jakarta";
		or05.jk="Laki-laki";
		or05.tptlahir="Jakarta";
		or05.umur=24;
		or05.cetak();
		

	}
	
	public static void makananFavorit() {
		System.out.println("1. Ikan Kuah Kuning");
		System.out.println("2. Papeda");
		System.out.println("3. Soto Daging");
		System.out.println("4. Nasi Goreng");
		System.out.println("5. Indomie");
	}

}

package hari02;

import java.util.Scanner;

public class Soal05 {
	
	static Scanner scn;

	public static void main(String[] args) {
		contoh0();
		System.out.println();
		contoh1();
	}
	
	public static void contoh0() {
		scn = new Scanner(System.in);// instansiasi/pembuatan objek baru untuk input
		System.out.print("Input 1 : ");// Cetak Input
		String text = scn.nextLine();//menampung input ke scn
		
		String[] array = text.split(" ");//Disimpan di satu array dan dipisah berdasarkan spasi
		for(int i = 0; i < array.length; i++) {//perulangan dimana i< dari panjang text
			String[] item = array[i].split("");//Menyimpan array i kedalam array string item dan di pisah setiap katanya
			for(int j = 0; j < item.length; j++) {//Perulangan dimana j < dari panjang text di item
				if(j>0 && j<item.length-1) {//Kondisi jika j lebih besar dari 0 sama dengan j kurang dari panjang text dikurang 1
					System.out.print("*");
				}else {
					System.out.print(item[j]);//Print satu kata
				}
			}
			System.out.print(" ");
		} 
	}


	public static void contoh1() {
		scn = new Scanner(System.in);
		System.out.print("Input 2 : ");
		String text = scn.nextLine();
		
		String[] array = text.split(" ");
		for(int i = 0; i < array.length; i++) {
			String[] item = array[i].split("");
			for(int j = 0; j < item.length; j++) {
				if(j>0 && j<item.length-1) {
					System.out.print("*");
				}else {
					System.out.print(item[j]);
				}
			}
			System.out.print(" ");
		}
	}
	
	
	}
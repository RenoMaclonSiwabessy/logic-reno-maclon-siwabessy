package hari03;

import java.util.Scanner;

import common.PrintArray;

public class Logic03Soal01 {
	
	static Scanner scn;

	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//1. Membuat array deret
		int[] deret = new int[n*4];
		int angka = o;
		for(int i = 0; i < deret.length; i++) {
			if(i%4==3) {
				deret[i] = m;
			}else {
			deret[i]=angka;
			angka = angka + m;
			}
		}
		
		//2. Buat array 2 dimensi
		String[][] array = new String[n][n];
		
		//4. Membuat index
		int index = 0;
		
		//2. Isi baris ke 0
		for(int i = 0; i < n; i++) {
			array[0][i] = deret[index]+"";
			index++;
		}
		
		//5. Isi kolom ke n - 1
		for(int j = 1; j < n; j++) {
			array[j][n-1] = deret[index]+"";
			index++;
		}
		
		//6. Isi baris ke n-1
		for(int z = n-2; z >= 0; z--) {
			array[n-1][z] = deret[index]+"";
			index++;
		}
		
		//7. Isi kolom ke 0 
		for(int i = 5; i > 0; i--) {
			array[i][0] = deret[index]+"";
			index++;
		}
		
		//memanggil array dari class array2D
		PrintArray.array2D(array);
			
	}

}

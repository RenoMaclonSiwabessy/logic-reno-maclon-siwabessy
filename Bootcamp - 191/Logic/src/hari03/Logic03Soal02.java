package hari03;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class Logic03Soal02 {
	
	static Scanner scn;

	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//1. Membuat array deret
		int[] vderet = DeretAngka.deret01(n*4, m, o);
		
		//2. Buat array 2 dimensi
		String[][] array = new String[n][n];
		
		//4. Mmebuat index
		int index = 0;
		
		//5. Diagonal
		for (int i = 0; i < n; i++) {
			array[n-1-i][i] = vderet[index]+"";
			index++;
			
		}
		
		//6. Kanan
		for(int i = 1; i < n; i++) {
			array[i][n-1] = vderet[index]+"";
			index++;
		}
		
		//7. Bawah
		for(int i = 5; i > 0; i--) {
			array[n-1][i] = vderet[index]+"";
			index++;
		}
		
		//menampilkannya
		PrintArray.array2D(array);
	}

}

package hari03;

import java.util.Scanner;

public class ArraySample01 {
	
	static Scanner scn;

	public static void main(String[] args) {
		// 3 1 9 3 15 5 => N = 6
		scn = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n = scn.nextInt();
		int angka1 = 3;
		int angka2 = 1;
		int[] array = new int[n];
		// Mengisi Array
		for(int i = 0;  i < array.length; i++) {
			if(i%2==0) {
				array[i]= angka1;
				angka1 = angka1 + 6;
			}else {
				array[i]= angka2;
				angka2 = angka2 + 2;
			}
			//tampilkan array ke layar
			System.out.print(array[i]+"\t");
		}
		
		System.out.println();
		 cobaJujur();
		 System.out.println();
		 cobaJujur1();
		 System.out.println();
		 cobaJujur2();
		

	}
	
	public static void cobaJujur() {
		// 3 1 9 3 15 5
		scn = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n = scn.nextInt();
		int a1 = 3;
		int a2 = 1;
		int[] array = new int[n];
		//Mengisi array
		for(int i = 0; i < array.length; i++) {
			if(i % 2 == 0) {
				array[i] = a1;
				a1 = a1 + 6;
			}else {
				array[i] = a2;
				a2 = a2 + 2;
			}
			//Output
			System.out.print(array[i]+"\t");
		}
	}
	
	
	public static void cobaJujur1() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n = scn.nextInt();
		int c1 = 3;
		int c2 = 1;
		int[] array = new int[n];
		
		for(int i = 0; i < array.length; i++) {
		if(i%2==0) {
			array[i] = c1;
			c1 = c1 + 6;
		}else {
			array[i] = c2;
			c2 = c2 + 2;
		}
		System.out.print(array[i]+"\t");
		}
		
		
	}
	
	public static void cobaJujur2() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n = scn.nextInt();
		int c1 = 3;
		int c2 = 1;
		int[] array = new int[n];
		for(int i = 0; i < array.length; i++) {
			if(i%2==0) {
				array[i] = c1;
				c1 = c1 + 6;
			}else {
				array[i] = c2;
				c2 = c2 + 2;
			}
			System.out.print(array[i]+"\t");
		}
	}
	
	public static void cobaJujur3() {
		// Intansiasi scn 
		scn = new Scanner(System.in); 
		System.out.print("Masukkan Nilai N5 : ");
		int n = scn.nextInt();
		// Variable array
		int[] array = new int[n];
		int e1 = 3;
		int e2 = 1;
		
		for(int i = 0; i < array.length; i++) {
			if(i % 2 == 0) {
				array[i] = e1;
				e1 = e1 + 6;
			}else {
				array[i] = e2;
				e2 = e2 + 2;
			}
			System.out.print(array[i] + "\t");
		}
	}
	
	public static void cobaJujur4() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan Nilai N6 : ");
		int n = scn.nextInt();
		int [] array = new int[n];
		int a1 = 3;
		int a2 = 1;
		
		for(int j = 0; j < array.length; j++){
			if(j % 2 == 0) {
				array[j] = a1;
				a1 = a1 + 6;
			}else {
				array[j] = a2;
				a2 = a2 + 2;
			}
			System.out.print(array[j] + "\t");
		}
	}
	
	public static void cobaJujur5() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan Nilai N7 : ");
		int n = scn.nextInt();
		int [] array = new int[n];
		int a1 = 3;
		int a2 = 1;
		
		for(int i = 0; i < array.length; i++) {
			if(i % 2 == 0) {
				array[i] = a1;
				a1 = a1 + 6;
			}else {
				array[i] = a2;
				a2 = a2 + 2;
			}
			System.out.print(array[i] + "\t");
		}
		
	}

}

package hari03;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class Logic03Soal02Mirror {
	
	static Scanner scn;

	public static void main(String[] args) {
		// Instansiasi
		scn = new Scanner(System.in);
		
		//Input n
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		//Input m
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		
		//Input o
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//Array Deret
		int[] deret = DeretAngka.deret01(n*4, m, o);
		
		//Instansiasi Untuk Array 2 Dimensi
		String[][] array = new String[n][n];
		
		//Index
		int index = 0;
		
		//Diagonal
		for(int i = 0; i < n; i++) {
			array[n-1-i][n-1-i] = deret[index]+"";
			index++;
		}
		
		//Kiri
		for (int i = 1; i < n; i++) {
			array[i][0] = deret[index]+"";
			index++;
		}
		
		//Bawah
		for(int i = 1; i < n - 1; i++) {
			array[n-1][i] = deret[index]+"";
			index++;
		}
		
		//Tampilan
		PrintArray.array2D(array);
		
		ulang1();
		ulang2();
		ulang3();
		ulang4();
	}
	
	public static void ulang1() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M1 : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O1 : ");
		int o = scn.nextInt();
		//Array Deret
		int[] deret1 = DeretAngka.deret01(n*4, m, o);
		//Array 2 Dimensi
		String[][] array1 = new String[n][n];
		//Index
		int index = 0;
		
		//Diagonal
		for (int i = 0; i < n; i++) {
			array1[n-1-i][n-1-i] = deret1[index]+"";
			index++;
		}
		//Kiri
		for (int i = 1; i < n; i++) {
			array1[i][0] = deret1[index]+"";
			index++;
		}
		//Bawah
		for (int i = 1; i < n-1; i++) {
			array1[n-1][i] = deret1[index]+"";
			index++;
		}
		//Tampilan
		PrintArray.array2D(array1);
	}
	
	public static void ulang2() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M2 : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O2 : ");
		int o = scn.nextInt();
		//Array Deret
		int[] deret2 = DeretAngka.deret01(n*4, m, o);
		//Array 2D
		String[][] array2 = new String[n][n];
		//Index
		int index = 0;
		//Diagonal
		for (int i = 0; i < n; i++) {
			array2[n-1-i][n-1-i] = deret2[index]+"";
			index++;
		}
		//Kiri
		for (int i = 1; i < n; i++) {
			array2[i][0] = deret2[index]+"";
			index++;
		}
		//Bawah
		for (int i = 1; i < n-1; i++) {
			array2[n-1][i] = deret2[index]+"";
			index++;
		}
		//Tampilan
		PrintArray.array2D(array2);
	}
	
	public static void ulang3() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M3 : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O3 : ");
		int o = scn.nextInt();
		//Array Deret
		int [] deret3 = DeretAngka.deret01(n*4, m, o);
		//Array 2D
		String [][] array3 = new String [n][n];
		//Index
		int index = 0;
		
		
		//Diagonal
		for (int i = 0; i < n; i++) {
			array3[n-1-i][n-1-i] = deret3[index]+"";
			index++;
		}
		//Kiri
		for (int i = 1; i < n; i++) {
			array3[i][0] = deret3[index]+"";
			index++;
		}
		//Bawah
		for (int i = 1; i < n - 1; i++) {
			array3[n-1][i] = deret3[index]+"";
			index++;
		}
	}
	
	public static void ulang4() {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M4 : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O4 : ");
		int o = scn.nextInt();
		int[] deret4 = DeretAngka.deret01(n*4, m, o);
		String[][] array4 = new String[n][n];
		int index = 0;
		
		//Diagonal
		for (int i = 0; i < n; i++) {
			array4[i-n-1][i-n-1] = deret4[index]+"";
			index++;
		}
		//Kiri
		for (int i = 1; i < n; i++) {
			array4[i][0] = deret4[index]+"";
			index++;
		}
		//Bawah
		for (int i = 1; i < n - 1; i++) {
			array4[n-1][i] = deret4[index]+"";
			index++;
		}
		//Tampilan
		PrintArray.array2D(array4);
	}

}
